#ifndef ARRAY2D_H
#define ARRAY2D_H

#include <iostream>
#include <iomanip>
#include <vector>
#include <stdexcept>
#include <string.h>
#include <fstream>
#include <functional>

#include "location.h"
#include "predicate_mask_iterator.h"

namespace array2d {
    // faster than std::function<bool(T)>
    template <class T> auto eq(const T& elem) -> decltype(std::bind1st(std::equal_to<T>(), elem)) {
        return std::bind1st(std::equal_to<T>(), elem);
    }

    template <class T> auto less(const T& elem) -> decltype(std::bind2nd(std::less<T>(), elem)) {
        return std::bind2nd(std::less<T>(), elem);
    }

    template <class T> std::function<bool(T)> eq3(const T& elem) {
        return [elem](const T& e) { return e == elem; };
    }

    template <class T> std::function<bool(T)> less3(const T& elem) {
        return [elem](const T& e) { return e < elem; };
    }
}


template <class T> class Array2d;
template <class Pred, class T> class ArrayView;
template <class T, class MaskIt> class ArrayMaskView;


template <class T> class Array2d {
    typedef const bool*  const_bool_iterator;
    typedef ArrayMaskView<T, const_bool_iterator> BoolArrayMaskView;

public:
    typedef T* iterator;
    typedef const T*  const_iterator;

    Array2d() : rows_(0), cols_(0) {
        init();
    }

    Array2d(int rows, int cols) : rows_(rows), cols_(cols) {
        init();
    }

    template <class It> Array2d(int rows, int cols, It start) : rows_(rows), cols_(cols) {
        init();
        std::copy(start, start+size_, begin());
    }

    // move constructor
    Array2d(Array2d&& a) :
            rows_(a.rows_),
            cols_(a.cols_),
            size_(a.size_),
            allocates_size_(a.allocates_size_),
            grid_(a.grid_) {

        a.rows_ = a.cols_ = a.size_ = a.allocates_size_ = 0;
        a.grid_ = nullptr;
    }

    void zero() {
        memset(grid_, 0, size_*sizeof(T));
    }

    Array2d<T> clone() const {
        return Array2d<T>(rows(), cols(), cbegin());
    }

    void set(const T& value) {
        for (int i = 0; i < size_; ++i) {
            grid_[i] = value;
        }
    }

    template <class Pred> void set(Pred pred, const T& value);

    void set(const Array2d<bool>& mask, const T& value);

    template <class MaskIt> void setByMask(MaskIt maskIt, const T& value);

    void resize(int rows , int cols);

    ~Array2d() { delete [] grid_; }

    template <class Pred> std::vector<Location> locations(Pred predicate) const;

    template <class Pred> int count(Pred predicate) const;

    template <class Fun> void apply(Fun fun);

    T& operator()(const Location& loc) {
        return (*this)(loc.row, loc.col);
    }

    const T& operator()(const Location& loc) const  {
        return (*this)(loc.row, loc.col);
    }

    T& operator()(int i, int j) {
        return const_cast<T&>(const_cast<const Array2d&>(*this).operator ()(i, j));
    }

    const T& operator()(int i, int j) const  {
        checkRange(i, j);
        return grid_[i*cols_ + j];
    }

    template <class Pred>
    ArrayMaskView<T, PredicateMaskIterator<Pred, const_iterator> > operator()(Pred pred) {
        return makeArrayView(*this, pred);
    }

    template <class Pred>
    const ArrayMaskView<T, PredicateMaskIterator<Pred, const_iterator> > operator()(Pred pred) const {
        //const ArrayMaskView won't modify anything
        return const_cast<Array2d&>(*this).operator()(pred);
    }

    const BoolArrayMaskView operator()(const Array2d<bool>& mask) const {
        if (mask.rows() == rows_ && mask.cols() == cols_) {
            return BoolArrayMaskView(const_cast<Array2d&>(*this), mask.begin());
        } else {
            char buf[100];
            sprintf(buf, "mask doesn't match matrix's' size: (%d, %d), (%d, %d)\n", mask.rows(),  mask.cols(), rows_, cols_);
            throw std::out_of_range(buf);
        }
    }

    BoolArrayMaskView operator()(const Array2d<bool>& mask) {
        return (const_cast<const Array2d<T>& >(*this).operator()(mask));
    }

    template <class T2, class Iter>
    const ArrayMaskView<T2, Iter> operator()(const ArrayMaskView<T2, Iter>& view) const {
        return ArrayMaskView<T2, Iter>(const_cast<Array2d&>(*this), view.mask_begin());
    }

    template <class T2, class Iter>
    ArrayMaskView<T2, Iter> operator()(const ArrayMaskView<T2, Iter>& view) {
        return (const_cast<const Array2d<T>& >(*this).operator()(view));
    }

    int rows() const { return rows_; }
    int cols() const { return cols_; }

    iterator begin() { return grid_; }
    iterator end() { return grid_ + size_; }

    const_iterator begin() const { return cbegin(); }
    const_iterator end() const { return cend(); }

    const_iterator cbegin() const { return grid_; }
    const_iterator cend() const { return grid_ + size_; }


   // const Array2d& operator=(const Array2d &&rhs);

private:
    int rows_;
    int cols_;
    int size_;
    int allocates_size_;
    T* grid_;

    // assignment and copy is not allowad for non-temporary variables
    Array2d(const Array2d& a);
    const Array2d& operator=(const Array2d &rhs);

    void checkRange(int i, int j) const;

    void init() {
        size_ = allocates_size_ = rows_*cols_;
        grid_ = new T[allocates_size_];
    }

    template <class Pred>
    ArrayMaskView<T, PredicateMaskIterator<Pred, const_iterator> > makeArrayView(Array2d<T>& array, Pred predicate) {
        return ArrayMaskView<T, PredicateMaskIterator<Pred, const_iterator> >(array, makePredicateMaskIterator(array.cbegin(), predicate));
    }

    template <class Pred, class TT> friend class ArrayView;
    template <class TE> friend class Array2d;

};




template <class T>
void Array2d<T>::set(const Array2d<bool>& mask, const T& value) {
    for (int i = 0; i < size_; ++i) {
        if (mask.grid_[i]) {
            grid_[i] = value;
        }
    }
}

template <class T>
template <class MaskIt>
void Array2d<T>::setByMask(MaskIt maskIt, const T& value) {
    for (int i = 0; i < size_; ++i) {
        if (*maskIt) {
            grid_[i] = value;
        }
        ++maskIt;
    }
}

template <class T>
template <class Fun>
void Array2d<T>::apply(Fun fun) {
    for (int i = 0; i < size_; ++i) {
        fun(grid_[i]);
    }
}

template <class T>
void Array2d<T>::checkRange(int i, int j) const {
#ifdef DEBUG
    if (!(i < rows_ && j < cols_) || i < 0 || j < 0) {
        char buf[100];
        sprintf(buf, "out_of_range: (%d, %d)/(%d, %d)\n", i, j, rows_, cols_);
        throw std::out_of_range(buf);
    }
#endif
}

template <class T>
template <class Pred>
void Array2d<T>::set(Pred pred, const T& value) {
    for (int i = 0; i < size_; ++i) {
        if (pred(grid_[i])) {
            grid_[i] = value;
        }
    }
}

template <class T>
template <class Pred>
std::vector<Location> Array2d<T>::locations(Pred pred) const {
    std::vector<Location> result;
    for (int i = 0; i < rows_; ++i) {
        for (int j = 0; j < cols_; ++j) {
            if (pred((*this)(i, j))) {
                result.push_back(Location(i, j));
            }
        }
    }
    return result;
}

template <class T>
template <class Pred>
int Array2d<T>::count(Pred predicate) const {
    int count = 0;
    for (int i = 0; i < size_; ++i) {
        if (predicate(grid_[i])) {
            count++;
        }
    }
    return count;
}


template <class T>
void Array2d<T>::resize(int rows , int cols) {
    if (rows*cols > allocates_size_) {
        allocates_size_ = rows * cols;
        delete [] grid_;
        grid_ = new T[allocates_size_];
    }
    size_ = rows*cols;
    rows_ = rows;
    cols_ = cols;
}

template <class T>
std::ostream& operator<<(std::ostream &os, const Array2d<T>& array2d) {
    using namespace std;

    for (int i = 0; i < array2d.rows(); ++i) {
        for (int j = 0; j < array2d.cols(); ++j) {
            os << setw(3) << array2d(i, j) << " ";
        }
        os << endl;
    }
    return os;
}


template<class T, class ArrayIt, class MaskIt> class MaskViewIterator {
    template <class P1, class P2> friend class ArrayMaskView;

    ArrayIt array_it_;
    ArrayIt array_end_;
    MaskIt mask_it_;

    MaskViewIterator(ArrayIt array_it, ArrayIt array_end, MaskIt mask_it)
        : array_it_(array_it),
          array_end_(array_end),
          mask_it_(mask_it) {

        if (array_it_ != array_end_ && !*mask_it_) {
            increment();
        }
    }

    void increment() {
        do {
            ++array_it_;
            ++mask_it_;
        } while (array_it_ != array_end_ && !*mask_it_);
    }

public:
    MaskViewIterator& operator++() {
        increment();
        return *this;
    }

    T& operator*() { return *array_it_; }

    bool operator==(const MaskViewIterator& other) const {
        return array_it_ == other.array_it_;
    }
    bool operator!=(const MaskViewIterator& other) const {
        return !(*this == other);
    }
};

template <class T, class MaskIt> class ArrayMaskView {
    template <class TT> friend class Array2d;

    Array2d<T>& array_;
    MaskIt mask_start_;

    ArrayMaskView(Array2d<T>& array, MaskIt mask_start) : array_(array), mask_start_(mask_start) {}

    MaskIt mask_begin() const { return mask_start_; }
public:
    typedef MaskViewIterator<T, typename Array2d<T>::iterator, MaskIt> iterator;
    typedef MaskViewIterator<const T, typename Array2d<T>::const_iterator, MaskIt> const_iterator;

    ArrayMaskView& operator=(const T& value) {
        array_.setByMask(mask_start_, value);
        return *this;
    }
    void set(const T& value) {
        array_.setByMask(mask_start_, value);
    }

    //std::vector<Location> locations() {}
    //template <class Pred> int count(Pred predicate) const;

    template <class Fun> void apply(Fun fun) {
        for (T& elem : *this) { fun(elem); }
    }

    template <class Fun> void apply(Fun fun) const {
        for (const T& elem : *this) { fun(elem); }
    }

    iterator begin() { return iterator(array_.begin(), array_.end(), mask_start_); }
    iterator end() { return iterator(array_.end(), array_.end(), mask_start_); }

    const_iterator begin() const { return cbegin(); }
    const_iterator end() const { return cend(); }

    const_iterator cbegin() const { return const_iterator(array_.begin(), array_.end(), mask_start_); }
    const_iterator cend() const { return const_iterator(array_.end(), array_.end(), mask_start_); }

    // conversion to mask
    operator Array2d<bool>() {
        return Array2d<bool> (array_.rows(), array_.cols(), mask_start_);
    }

    operator const Array2d<bool>() const {
        return Array2d<bool> (array_.rows(), array_.cols(), mask_start_);
    }
};

#endif // ARRAY2D_H
