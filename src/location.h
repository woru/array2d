#ifndef LOCATION_H_
#define LOCATION_H_

struct Location {
    int row, col;

    Location() {
        row = col = -1;
    }

    Location(int _row, int _col) : row(_row), col(_col) {}

    bool valid() const {
        return row != -1 && col != -1;
    }

    bool operator ==(const Location &other) const {
        return other.row == row && other.col == col;
    }

    bool operator !=(const Location &other) const {
        return !(*this == other);
    }

    bool operator<(const Location &other) const {
        if (row < other.row)
            return true;
        if (other.row == row)
            return col < other.col;
        return false;
    }
};


inline std::ostream& operator<<(std::ostream &os, const Location &location) {
    return os << "(" << location.row << ", " << location.col << ")";
}


struct LocationHash {
    std::size_t operator()(const Location &location) const {
        return (location.row << 16) + location.col + 13;
    }
};

#endif //LOCATION_H_

