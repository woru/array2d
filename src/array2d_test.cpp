#include <iostream>
#include <string>
#include <functional>
#include "gtest/gtest.h"
#include "array2d.h"

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::function;
using namespace array2d;

TEST(Array2dTest, IndexByInts) {
    Array2d<int> a(2, 2);
    a(0, 0) = 1;
    a(0, 1) = 2;
    a(1, 0) = 3;
    a(1, 1) = 4;

    EXPECT_EQ(1, a(0, 0));
    EXPECT_EQ(2, a(0, 1));
    EXPECT_EQ(3, a(1, 0));
    EXPECT_EQ(4, a(1, 1));
}

TEST(Array2dTest, CreateFromArray) {
    double tab[] = {
        1.1, 2.2,
        3.3, 4.3
    };
    Array2d<double> fromArray(2, 2, tab);

    EXPECT_EQ(1.1, fromArray(0, 0));
    EXPECT_EQ(2.2, fromArray(0, 1));
    EXPECT_EQ(3.3, fromArray(1, 0));
    EXPECT_EQ(4.3, fromArray(1, 1));
}

TEST(Array2dTest, IndexByLocation) {
    Array2d<int> a(2, 2);

    a(Location(1, 0)) = 1;

    EXPECT_EQ(1, a(1, 0));
}

TEST(Array2dTest, Count) {
    Array2d<int> a(2, 2);
    a(0, 0) = 1;
    a(0, 1) = 2;
    a(1, 0) = 1;
    a(1, 1) = 4;

    EXPECT_EQ(2, a.count(eq(1)));
}


TEST(Array2dTest, Set) {
    Array2d<int> a(2, 2);

    a.set(3);

    EXPECT_EQ(3, a(0, 0));
    EXPECT_EQ(3, a(0, 1));
    EXPECT_EQ(3, a(1, 0));
    EXPECT_EQ(3, a(1, 1));
}

TEST(Array2dTest, Resize) {
    Array2d<int> a(2, 2);

    a.resize(2, 3);

    EXPECT_EQ(2, a.rows());
    EXPECT_EQ(3, a.cols());
}

TEST(Array2dTest, Locations) {
    Array2d<int> a(2, 2);
    a(0, 0) = 1;
    a(0, 1) = 2;
    a(1, 0) = 1;
    a(1, 1) = 4;

    EXPECT_EQ(2, a.locations(eq(1)).size());
    EXPECT_EQ(Location(0, 0), a.locations(eq(1))[0]);
    EXPECT_EQ(Location(1, 0), a.locations(eq(1))[1]);
}


TEST(Array2dTest, AssignmentToPredicate) {
    Array2d<int> a(2, 2);
    a(0, 0) = 1;
    a(0, 1) = 2;
    a(1, 0) = 1;
    a(1, 1) = 4;

    a(eq(1)) = 7;

    EXPECT_EQ(7, a(0, 0));
    EXPECT_EQ(2, a(0, 1));
    EXPECT_EQ(7, a(1, 0));
    EXPECT_EQ(4, a(1, 1));
}

TEST(Array2dTest, AssignmentToMask) {
    // given
    bool mtab[] = {
        true, false,
        false, true
    };
    Array2d<bool> mask(2, 2, mtab);

    double tab[] = {
        1.1, 2.2,
        3.3, 4.3
    };
    Array2d<double> ar(2, 2, tab);

    // when
    ar(mask) = 5.5;

    // then
    EXPECT_EQ(5.5, ar(0, 0));
    EXPECT_EQ(2.2, ar(0, 1));
    EXPECT_EQ(3.3, ar(1, 0));
    EXPECT_EQ(5.5, ar(1, 1));
}

TEST(Array2dTest, ConversionToMask) {
    // given
    double tab[] = {
        1.1, 2.2,
        3.3, 4.3
    };
    Array2d<double> ar(2, 2, tab);

    // when
    Array2d<bool> mask = ar(less(3));

    // then
    ASSERT_TRUE(mask(0, 0));
    ASSERT_TRUE(mask(0, 1));
    ASSERT_FALSE(mask(1, 0));
    ASSERT_FALSE(mask(1, 1));
}

TEST(Array2dTest, Apply) {
    // given
    double tab[] = {
        1.1, 4.2,
        3.3, 4.3
    };
    Array2d<double> ar(2, 2, tab);

    // when
    ar(less(4.0)).apply([](double& elem) {elem += 1;});

    // then
    EXPECT_EQ(2.1, ar(0, 0));
    EXPECT_EQ(4.2, ar(0, 1));
    EXPECT_EQ(4.3, ar(1, 0));
    EXPECT_EQ(4.3, ar(1, 1));
}

TEST(Array2dTest, Clone) {
    // given
    double tab[] = {
        1.1, 4.2,
        3.3, 4.3
    };
    Array2d<double> ar(2, 2, tab);
    Array2d<double> clone = ar.clone();


    // when
    clone(less(4.0)) = 2.2;

    // then
    EXPECT_EQ(2.2, clone(0, 0));
    EXPECT_EQ(4.2, clone(0, 1));
    EXPECT_EQ(2.2, clone(1, 0));
    EXPECT_EQ(4.3, clone(1, 1));

    EXPECT_EQ(1.1, ar(0, 0));
    EXPECT_EQ(4.2, ar(0, 1));
    EXPECT_EQ(3.3, ar(1, 0));
    EXPECT_EQ(4.3, ar(1, 1));
}

TEST(Array2dTest, SetOnArrayView) {
    // given
    double tab[] = {
        1.1, 4.2,
        3.3, 4.3
    };
    Array2d<double> ar(2, 2, tab);

    // when
    ar(less(4)).set(1.2);

    // then
    EXPECT_EQ(1.2, ar(0, 0));
    EXPECT_EQ(4.2, ar(0, 1));
    EXPECT_EQ(1.2, ar(1, 0));
    EXPECT_EQ(4.3, ar(1, 1));
}

TEST(Array2dTest, AssignmentToMaskFromPredicate) {
    // given
    double tab[] = {
        1.1, 4.2,
        3.3, 4.3
    };
    Array2d<double> ar(2, 2, tab);

    // when
    ar(ar(less(4))) = 1.2;

    // then
    EXPECT_EQ(1.2, ar(0, 0));
    EXPECT_EQ(4.2, ar(0, 1));
    EXPECT_EQ(1.2, ar(1, 0));
    EXPECT_EQ(4.3, ar(1, 1));
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
