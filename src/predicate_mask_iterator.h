#ifndef PREDICATE_MASK_ITERATOR_H
#define PREDICATE_MASK_ITERATOR_H

template<class Pred, class It> class PredicateMaskIterator
            : public std::iterator< std::bidirectional_iterator_tag, bool> {
    It start_;
    Pred predicate_;
public:

    PredicateMaskIterator(It start, Pred predicate)
        : start_(start),
          predicate_(predicate) {}

    PredicateMaskIterator& operator++() {
        ++start_;
        return *this;
    }

    PredicateMaskIterator& operator--() {
        --start_;
        return *this;
    }

    //TODO: +=, -, etc
    PredicateMaskIterator operator+(int offset) const{
        return PredicateMaskIterator(start_+offset, predicate_);
    }

    bool operator*() { return predicate_(*start_); }

    bool operator==(const PredicateMaskIterator& other) const {
        return start_ == other.start_;
    }
    bool operator!=(const PredicateMaskIterator& other) const {
        return !(*this == other);
    }
};

template<class Pred, class It>
PredicateMaskIterator<Pred, It> makePredicateMaskIterator(It array_it, Pred pred) {
    return PredicateMaskIterator<Pred, It>(array_it, pred);
}

#endif // PREDICATE_MASK_ITERATOR_H
