

merge ArrayView and ArrayMaskView

ar(mask).apply(add(4));
ar(mask).set(5);
ar(mask).locations()

ar(less(4)).apply(add(4));
ar(less(4)).set(5);
ar(less(4)).locations()


swap

clone

Array2d<bool> mask2(2, 2, {
   true, false,
   false, true});
