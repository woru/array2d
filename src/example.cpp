#include <iostream>
#include <string>

#include "array2d.h"
#include "Timer.h"

#define ASSERT_TRUE(a) assertTrue(a, #a)
using std::string;
using std::cout;
using std::cerr;
using std::endl;
using namespace array2d;


void assertTrue(bool cond, const string& msg) {
    if (!cond) {
        cerr << "Failed: " << msg << endl;
        exit(1);
    }
}



void testIndexPerformance() {
    using namespace std;
    pair<int,int> ** na = new pair<int,int> *[9000];
    for (int i = 0; i < 9000; ++i) {
        na[i] = new pair<int,int>[9000];
    }

    Timer timer;
    timer.start();
    Array2d<pair<int,int> > b(9000, 9000);
    Array2d<pair<int,int> > c(9000, 9000);

    for (int i = 0; i < b.rows(); ++i) {
        for (int j = 0; j < b.cols(); ++j) {
            b(i, j).first = i;
            b(i, j).second = j;
            c(j, i).first = i;
            c(j, i).second = j;
        }
    }
    cout << timer.getTime() << endl;


    for (int t = 0; t < 3; ++t) {
        timer.start();
        for (int i = 0; i < b.rows(); ++i) {
            for (int j = 0; j < b.cols(); ++j) {
                b(i, j).first = i + b(j, i).second;
                b(i, j).second = j + b(j, i).first;
            }
        }
        cout << timer.getTime() << endl;

        timer.start();
        for (int i = 0; i < b.rows(); ++i) {
            for (int j = 0; j < b.cols(); ++j) {
                na[i][j].first = i + na[j][i].second;
                na[i][j].second = j + na[j][i].first;
            }
        }
        cout << timer.getTime() << endl;
    }
}

void testPredicateIndexPerformance() {
    using namespace std;
    Timer timer;
    Array2d<pair<int,int> > a(9000, 9000);

    for (int t = 0; t < 3; ++t) {
        for (int i = 0; i < a.rows(); ++i) {
            for (int j = 0; j < a.cols(); ++j) {
                if (i < j) {
                    a(i, j) = make_pair(1, 2);
                } else {
                    a(i, j) = make_pair(1, 1);
                }
            }
        }
        timer.start();
        a(eq(make_pair(1, 1))) = make_pair(1, 4);
        cout << timer.getTime() << endl;
    }

    for (int t = 0; t < 3; ++t) {
        for (int i = 0; i < a.rows(); ++i) {
            for (int j = 0; j < a.cols(); ++j) {
                if (i < j) {
                    a(i, j) = make_pair(1, 2);
                } else {
                    a(i, j) = make_pair(1, 1);
                }
            }
        }
        timer.start();
        a(eq3(make_pair(1, 1))) = make_pair(1, 4);
        cout << timer.getTime() << endl;
    }

    for (int t = 0; t < 3; ++t) {
        for (int i = 0; i < a.rows(); ++i) {
            for (int j = 0; j < a.cols(); ++j) {
                if (i < j) {
                    a(i, j) = make_pair(1, 2);
                } else {
                    a(i, j) = make_pair(1, 1);
                }
            }
        }
        timer.start();

        for (pair<int,int>& p : a) {
            auto p1 = make_pair(1, 1);
            auto p2 = make_pair(1, 4);
            if (p == p1) {
                p = p2;
            }
        }
        cout << timer.getTime() << endl;
    }
}



void testIndex() {
    Array2d<int> a(2, 2);
    a(eq(1)) = 5;
}


int main() {
    Array2d<int> def;

    double tab[] = {
        1.1, 2.2,
        3.3, 4.3
    };
    Array2d<double> fromArray(2, 2, tab);
    cout << fromArray << endl;

    Array2d<int> a(2, 2);
    a(0, 0) = 1;
    a(0, 1) = 2;
    a(1, 0) = 3;
    a(1, 1) = 4;
    cout << a << endl;

    ASSERT_TRUE(a(0, 0) == 1);
    ASSERT_TRUE(a(0, 1) == 2);
    ASSERT_TRUE(a(1, 0) == 3);
    ASSERT_TRUE(a(1, 1) == 4);


    a(Location(1, 0)) = 1;
    ASSERT_TRUE(a(1, 0) == 1);
    ASSERT_TRUE(a.count(eq(1)) == 2);

    a.set(5);

    ASSERT_TRUE(a(0, 0) == 5);
    ASSERT_TRUE(a(0, 1) == 5);
    ASSERT_TRUE(a(1, 0) == 5);
    ASSERT_TRUE(a(1, 1) == 5);

    cout << a << endl;

    a.resize(2, 3);

    ASSERT_TRUE(a.rows() == 2);
    ASSERT_TRUE(a.cols() == 3);

    a(0, 0) = 1;
    a(0, 1) = 2;
    a(0, 2) = 3;
    a(1, 0) = 4;
    a(1, 1) = 5;
    a(1, 2) = 6;
    cout << a << endl;

    a(0, 2) = 1;
    cout << a << endl;

    for (const Location& loc : a.locations(eq(1))) {
        cout << loc << "\n";
    }

    ASSERT_TRUE(a.locations(eq(1)).size() == 2);
    ASSERT_TRUE(a.locations(eq(1))[0] == Location(0, 0));
    ASSERT_TRUE(a.locations(eq(1))[1] == Location(0, 2));

    for (int& i : a(less(2))) {
        cout << i << endl;
    }

    a(eq(1)) = 7;

    cout << a << endl;

    ASSERT_TRUE(a(0, 0) == 7);
    ASSERT_TRUE(a.count(eq(7)) == 2);

    const Array2d<double> c(2, 2, tab);
    for (const double& i : c(less(2.0))) {
        cout << i << endl;
    }


    bool mtab[] = {
        true, false,
        false, true
    };
    Array2d<bool> mask(2, 2, mtab);
    Array2d<double> ar(2, 2, tab);

    cout << ar << "\n";

    ar(mask) = 5.5;

    cout << ar << "\n";

    ASSERT_TRUE(ar(0, 0) == 5.5);
    ASSERT_TRUE(ar(0, 1) == 2.2);
    ASSERT_TRUE(ar(1, 0) == 3.3);
    ASSERT_TRUE(ar(1, 1) == 5.5);

    cout << "mask:\n";
    for (double& d : ar(mask)) {
        cout << d << endl;
    }

    cout << "mask from predicate:\n";
    cout << ar << "\n";

    Array2d<bool> mask_form_pred = ar(less(3));
    cout << mask_form_pred << "\n";
    cout << "\n";


    cout << ar << "\n";

    ar(ar(less(3.4))) = 3.5;

    cout << ar << "\n";

    ASSERT_TRUE(ar(0, 0) == 5.5);
    ASSERT_TRUE(ar(0, 1) == 3.5);
    ASSERT_TRUE(ar(1, 0) == 3.5);
    ASSERT_TRUE(ar(1, 1) == 5.5);

    ar(less(3.6)).apply([](double& elem) {elem += 1;});

    cout << ar << "\n";

    ASSERT_TRUE(ar(0, 0) == 5.5);
    ASSERT_TRUE(ar(0, 1) == 4.5);
    ASSERT_TRUE(ar(1, 0) == 4.5);
    ASSERT_TRUE(ar(1, 1) == 5.5);


    ar(less(5)).set(1.1);

    cout << ar << "\n";

    ASSERT_TRUE(ar(0, 0) == 5.5);
    ASSERT_TRUE(ar(0, 1) == 1.1);
    ASSERT_TRUE(ar(1, 0) == 1.1);
    ASSERT_TRUE(ar(1, 1) == 5.5);



    const Array2d<double>& car = ar;
    // car(less(5)).apply([](double& elem) {elem += 1;}); doesn't compile

    car(less(5)).apply([](const double& elem) {cout << elem << "\n";});

    car(less(5)).apply([](double elem) {cout << elem << "\n";});



    testPredicateIndexPerformance();

    return 0;
}
